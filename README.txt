Example of compile command for hw1a:
gcc -std=c99 -pedantic -Wall -Wextra hw1a.c

Example of compile command for hw1b:
gcc -std=c99 -pedantic -Wall -Wextra hw1b.c

Example of execution command for hw1a.c:
./a.out courseData.txt

Example of execution command for hw1b.c:
./a.out

WHAT DOES hw1a.c DO?
hw1a.c takes in courseData.txt (input file name line by line, converts each line to decimal by turning each component of the line into bit and then packing them together into one decimal int. The program then outputs to the screen the original input line, the decimal version, and the binary version. Also, it prints to a file called courseInts. txt the decimal versions of the inputs.

WHAT DOES hw1b.c DO?
hw1b.c takes in courseInts.txt line by line and stores them in an array of (unsigned) ints. The program then prompts for user input and returns back class results, gpa, number of classes, etc. based on the user input value.
