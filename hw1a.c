/*
Michael Ashmead, 600.120, 2/9/15, Homework 1, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#define _GNU_SOURCE
#define LENGTH 16

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void printNumber(char[]);
unsigned int addToDecimal(unsigned int, unsigned int, int);
void toBinary(unsigned int);

int main(int argc, char* argv[]) {
   char course[LENGTH];
   if (argc == 1) {
      printf("Please enter a file name!\n");
      return 1;  // exit program
   }

   strncpy(course, argv[1], LENGTH);  // copy to course string
   course[LENGTH-1] = '\0';   // make sure last character is null
   
   //read lines from file
   FILE* fp;
   char* line = NULL;
   size_t len = 0;
   ssize_t read;

   fp = fopen(course, "r");
   if (fp == NULL)
      exit(EXIT_FAILURE);

   //write decimals to file
   FILE* decimals;
   decimals = fopen("courseInts.txt", "w");

   while((read = getline(&line, &len, fp)) != -1) {
      printNumber(line);
   }

   fclose(decimals);

   fclose(fp);
   if (line)
      free(line);
   exit(EXIT_SUCCESS);
   
   return 0;
}

//get course division number
void printNumber(char course[]) {
   //remove newline character from course
   if (strlen(course) > 14) {
      char tempCourse[15];
      int x;
      for (x = 0; x < 15; x++) {
         tempCourse[x] = course[x];

      }
      course = tempCourse;
   }
   printf("%s ", course);

   unsigned int divisionNum;
   if (course[0] == 'A') {
      divisionNum = 0;
   }
   else if (course[0] == 'B') {
      divisionNum = 1;
   }
   else if (course[0] == 'E' && course[1] == 'D') {
      divisionNum = 2;
   }
   else if (course[0] == 'E' && course[1] == 'N') {
      divisionNum = 3;
   }
   else if (course[0] == 'M') {
      divisionNum = 4;
   }
   else if (course[0] == 'P' && course[1] == 'H') {
      divisionNum = 5;
   }
   else if (course[0] == 'P' && course[1] == 'Y') {
      divisionNum = 6;
   }
   else {
      divisionNum = 7;
   }

   //get course department number
   unsigned int departmentNumber;
   departmentNumber = 100 * (course[3] - '0') + 10 * (course[4] - '0') + 1 * (course[5] - '0');

   //get course number
   unsigned int courseNumber;
   courseNumber = 100 * (course[7] - '0') + 10 * (course[8] - '0') + 1 * (course[9] - '0');

   //get first part of course grade
   unsigned int gradeFirstPart;
   switch (course[10]) {
      case 'A':
         gradeFirstPart = 0; break;
      case 'B':
         gradeFirstPart = 1; break;
      case 'C':
         gradeFirstPart = 2; break;
      case 'D':
         gradeFirstPart = 3; break;
      case 'F':
         gradeFirstPart = 4; break;
      case 'I':
         gradeFirstPart = 5; break;
      case 'S':
         gradeFirstPart = 6; break;
      default:
         gradeFirstPart = 7;
   }

   //get second part of course grade
   unsigned int gradeSecondPart;
   if (course[11] == '+')
      gradeSecondPart = 0;
   else if (course[11] == '-')
      gradeSecondPart = 1;
   else
      gradeSecondPart = 2;

   //get first part of credits
   unsigned int creditFirstPart;
   creditFirstPart = course[12] - '0';

   //get second part of credits
   unsigned int creditSecondPart;
   if (course[14] == '0')
      creditSecondPart = 0;
   else
      creditSecondPart = 1;

   //make final decimal number with bitwise operations
   unsigned int finalDecimal = 0;
   finalDecimal = addToDecimal(finalDecimal, divisionNum, 3);
   finalDecimal = addToDecimal(finalDecimal, departmentNumber, 10);
   finalDecimal = addToDecimal(finalDecimal, courseNumber, 10);
   finalDecimal = addToDecimal(finalDecimal, gradeFirstPart, 3);
   finalDecimal = addToDecimal(finalDecimal, gradeSecondPart, 2);
   finalDecimal = addToDecimal(finalDecimal, creditFirstPart, 3);
   finalDecimal = addToDecimal(finalDecimal, creditSecondPart, 1);

   printf(" %u ", finalDecimal);

   //print finalDecimal to courseInts.txt
   FILE* update = fopen("courseInts.txt", "a");
   fprintf(update, "%u\n", finalDecimal);
   fclose(update);

   //convert decimal to binary and print
   toBinary(finalDecimal);
   printf("\n");
}


void toBinary(unsigned int input) {
   printf(" ");
   int shift, temp;
   for (shift = 31; shift >=0; shift--) {
      temp = input >> shift;
      if (temp & 1) {
         printf("1");
      }
      else {
         printf("0");
      }
   }
}

unsigned int addToDecimal(unsigned int original, unsigned int adding, int bits) {
   original = original << bits;
   original = original | adding;
   return original;
}
