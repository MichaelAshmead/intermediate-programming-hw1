/*
Michael Ashmead, 600.120, 2/9/15, Homework 1, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#define _GNU_SOURCE
#define LENGTH 16

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int toInteger(char[]);
void mainPrompt();
void displayTotalCourses(unsigned int[]);
void listDepartmentCourses(unsigned int[]);
void listGradeCourses(unsigned int[]);
void listCreditCourses(unsigned int[]);
void calcGPA(unsigned int[]);
double getPoints(char[]);
double getCredits(unsigned int);
void makeReadable(unsigned int);

int main(void) {
   //read lines from file
   FILE* fp;
   char* line = NULL;
   size_t len = 0;
   ssize_t read;

   fp = fopen("courseInts.txt", "r");
   if (fp == NULL)
      exit(EXIT_FAILURE);

   //put each decimal value into array of int
   unsigned int courseInts[2500] = {0};
   //set to 12 because this is a binary value of 00000000000000000000000000001100 which is impossible to have
   for (int x = 0; x < 2500; x++)
      courseInts[x] = 12;

   int counter = 0;
   while((read = getline(&line, &len, fp)) != -1) {
      courseInts[counter] = toInteger(line);
      counter++;
   }

   //prompt options
   char input[256];
   do {
      mainPrompt();
      scanf("%s", input);
      switch (input[0]) {
         case 'n':
            displayTotalCourses(courseInts); break;
         case 'd':
            listDepartmentCourses(courseInts); break;
         case 'l':
            listGradeCourses(courseInts); break;
         case 'c':
            listCreditCourses(courseInts); break;
         case 'g':
            calcGPA(courseInts);
      }
   } while(input[0] != 'q');

   fclose(fp);
   if (line)
      free(line);
   exit(EXIT_SUCCESS);
   
   return 0;
}

int toInteger(char input[]) {
   int c, sign, offset, n;

   if (input[0] == '-')
      sign = -1;
   
   if (sign == -1)
      offset = 1;
   else
      offset = 0;

   n = 0;

   for (c = offset; input[c] != '\0' && input[c] != '\n'; c++) {
      n = n * 10 + input[c] - '0';
   }

   if (sign == -1)
      n = -n;

   return n;
}

void mainPrompt() {
   printf("%s\n", "n - display the total number of courses");
   printf("%s\n", "d - list all courses from a particular department (prompt for number)");
   printf("%s\n", "l - list all courses with a particular letter grade (prompt for grade)");
   printf("%s\n", "c - list all courses with at least a specified number of credits (prompt for credits)");
   printf("%s\n", "g - compute the GPA of all the courses with letter grades (skipping those with 'I', 'S' or 'U' grades");
   printf("%s\n", "q - quit the program");
   printf("%s ", "Enter letter choice ->");
}

void displayTotalCourses(unsigned int courses[]) {
   int counter = 0;
   for (int x = 0; x < 2500; x++) {
      if (courses[x] != 12) {
         counter++;
      }
   }
   printf("%i\n", counter);
}

void listDepartmentCourses(unsigned int courses[]) {
   printf("%s ", "Number?");
   unsigned int input;
   scanf("%u", &input);
   for (int x = 0; x < 2500; x++) {
      if (courses[x] != 12) {
         unsigned int shifted = courses[x] >> 19;
         if (((shifted ^ input) << 22) == 0) {
            makeReadable(courses[x]);
         }
      }
   }
}

void listGradeCourses(unsigned int courses[]) {
   printf("%s ", "Grade?");
   char input[256];
   scanf("%s", input);
   input[0] = toupper(input[0]); //make char upper case

   unsigned int toLookFor;
   if (input[0] == 'A' && input[1] == '+')
      toLookFor = 0; //for 00000
   else if (input[0] == 'A' && input[1] == '-')
      toLookFor = 1; //for 00001
   else if (input[0] == 'A' && input[1] != '+' && input[1] != '-')
      toLookFor = 2; //for 00010
   else if (input[0] == 'B' && input[1] == '+')
      toLookFor = 4; //for 00100
   else if (input[0] == 'B' && input[1] == '-')
      toLookFor = 5; //for 00101
   else if (input[0] == 'B' && input[1] != '+' && input[1] != '-')
      toLookFor = 6; //for 00110
   else if (input[0] == 'C' && input[1] == '+')
      toLookFor = 8; //for 01000
   else if (input[0] == 'C' && input[1] == '-')
      toLookFor = 9; //for 01001
   else if (input[0] == 'C' && input[1] != '+' && input[1] != '-')
      toLookFor = 10; //for 01010
   else if (input[0] == 'D' && input[1] == '+')
      toLookFor = 12; //for 01100
   else if (input[0] == 'D' && input[1] == '-')
      toLookFor = 13; //for 01101
   else if (input[0] == 'D' && input[1] != '+' && input[1] != '-')
      toLookFor = 14; //for 01110
   else if (input[0] == 'F' && input[1] == '+')
      toLookFor = 16; //for 10000
   else if (input[0] == 'F' && input[1] == '-')
      toLookFor = 17; //for 10001
   else if (input[0] == 'F' && input[1] != '+' && input[1] != '-')
      toLookFor = 18; //for 10010
   else if (input[0] == 'I' && input[1] == '+')
      toLookFor = 20; //for 10100
   else if (input[0] == 'I' && input[1] == '-')
      toLookFor = 21; //for 10101
   else if (input[0] == 'I' && input[1] != '+' && input[1] != '-')
      toLookFor = 22; //for 10110
   else if (input[0] == 'S' && input[1] == '+')
      toLookFor = 24; //for 11000
   else if (input[0] == 'S' && input[1] == '-')
      toLookFor = 25; //for 11001
   else if (input[0] == 'S' && input[1] != '+' && input[1] != '-')
      toLookFor = 26; //for 11010
   else if (input[0] == 'U' && input[1] == '+')
      toLookFor = 28; //for 11100
   else if (input[0] == 'U' && input[1] == '-')
      toLookFor = 29; //for 11101
   else
      toLookFor = 30; //for 11110

   for (int x = 0; x < 2500; x++) {
      if (courses[x] != 12) {
         unsigned int shifted = courses[x] >> 4;
         if (((shifted ^ toLookFor) << 27) == 0) {
            makeReadable(courses[x]);
         }
      }
   }
}

void listCreditCourses(unsigned int courses[]) {
   printf("%s ", "Credits?");
   char input[256];
   scanf("%s", input);

   unsigned int threshold;
   if (input[0] == '0' && input[2] == '0')
      threshold = 0; //for 0000...
   else if (input[0] == '0' && input[2] == '5')
      threshold = 1; //for 0001
   else if (input[0] == '1' && input[2] == '0')
      threshold = 2; //for 0010
   else if (input[0] == '1' && input[2] == '5')
      threshold = 3; //for 0011
   else if (input[0] == '2' && input[2] == '0')
      threshold = 4; //for 0100
   else if (input[0] == '2' && input[2] == '5')
      threshold = 5; //for 0100
   else if (input[0] == '3' && input[2] == '0')
      threshold = 6; //for 0110
   else if (input[0] == '3' && input[2] == '5')
      threshold = 7; //for 0111
   else if (input[0] == '4' && input[2] == '0')
      threshold = 8; //for 1000
   else if (input[0] == '4' && input[2] == '5')
      threshold = 9; //for 1001
   else if (input[0] == '5' && input[2] == '0')
      threshold = 10; //for 1010
   else
      threshold = 11; //for 1011

   for (int x = 0; x < 2500; x++) {
      if (courses[x] != 12) {
         unsigned int shifted = (courses[x] & 15);
         if (shifted >= threshold) {
            makeReadable(courses[x]);
         }
      }
   }
}

void calcGPA(unsigned int courses[]) {
   double totalpoints = 0.0;
   double totalcredits = 0.0;
   for (int x = 0; x < 2500; x++) {
      if (courses[x] != 12) {
         unsigned int shifted = courses[x] >> 4;
         if (((shifted ^ 0) << 27) == 0) { //for 00000
            totalpoints += (getPoints("A+") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 1) << 27) == 0) { //for 00001
            totalpoints += (getPoints("A-") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 2) << 27) == 0) { //for 00010
            totalpoints += (getPoints("A") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 4) << 27) == 0) { //for 00100
            totalpoints += (getPoints("B+") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 5) << 27) == 0) { //for 00101
            totalpoints += (getPoints("B-") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 6) << 27) == 0) { //for 00110
            totalpoints += (getPoints("B") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 8) << 27) == 0) { //for 01000
            totalpoints += (getPoints("C+") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 9) << 27) == 0) { //for 01001
            totalpoints += (getPoints("C-") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 10) << 27) == 0) { //for 01010
            totalpoints += (getPoints("C") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 12) << 27) == 0) { //for 01100
            totalpoints += (getPoints("D+") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 13) << 27) == 0) { //for 01101
            totalpoints += (getPoints("D-") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 14) << 27) == 0) { //for 01110
            totalpoints += (getPoints("D") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 16) << 27) == 0) { //for 10000
            totalpoints += (getPoints("F+") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else if (((shifted ^ 17) << 27) == 0) { //for 10001
            totalpoints += (getPoints("F-") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
         else { //for 10010
            totalpoints += (getPoints("F") * getCredits(courses[x]));
            totalcredits += getCredits(courses[x]);
         }
      }
   }
   printf("%4.2f\n", totalpoints / totalcredits);
}

double getPoints(char g[]) {
   double points = 0;
   switch (g[0]) {
      case 'A': points = 4; break;
      case 'B': points = 3; break;
      case 'C': points = 2; break;
      case 'D': points = 1; break;
      default: points = 0;
   }

   if (g[1] && 'B' <= g[0] && g[0] <= 'D')
      points += .3;
   if (g[1] == '-' && 'A' <= g[0] && g[0] <= 'C')
      points -= .3;
   return points;
}

double getCredits(unsigned int course) {
   unsigned int shifted = (course << 27);
   switch (shifted) {
      case 0: //for 0000...
         return 0.0; break;
      case 268435456: //for 0001...
         return 0.5; break;
      case 536870912: //for 0010...
         return 1.0; break;
      case 805306368: //for 0011...
         return 1.5; break;
      case 1073741824: //for 0100...
         return 2.0; break;
      case 1342177280: //for 0101...
         return 2.5; break;
      case 1610612736: //for 0110...
         return 3.0; break;
      case 1879048192: //for 0111...
         return 3.5; break;
      case 2147483648: //for 1000...
         return 4.0; break;
      case 2415919104: //for 1001...
         return 4.5; break;
      case 2684354560: //for 1010...
         return 5.0; break;
      default: //for 1011...
         return 5.5;
   }
}

void makeReadable(unsigned int decimal) {
   char finalString[15];
   unsigned int division = ((decimal >> 29) << 29);
   switch (division) {
      case 0: //for 000
         finalString[0] = 'A';
         finalString[1] = 'S';
         break;
      case 536870912: //for 001
         finalString[0] = 'B';
         finalString[1] = 'U';
         break;
      case 1073741824: //for 010
         finalString[0] = 'E';
         finalString[1] = 'D';
         break;
      case 1610612736: //for 011
         finalString[0] = 'E';
         finalString[1] = 'N';
         break;
      case 2147483648: //for 100
         finalString[0] = 'M';
         finalString[1] = 'E';
         break;
      case 2684354560: //for 101
         finalString[0] = 'P';
         finalString[1] = 'H';
         break;
      case 3221225472: //for 110
         finalString[0] = 'P';
         finalString[1] = 'Y';
         break;
      default: //for 111
         finalString[0] = 'S';
         finalString[1] = 'A';
   }

   finalString[2] = '.';

   //add department to string
   unsigned int department = (decimal & 536346624); //get 10 department bits
   department = department >> 19;
   int thirddigit = department % 10;
   int seconddigit = (department / 10) % 10;
   int firstdigit = department / 100;
   finalString[3] = (char)(((int)'0')+firstdigit);
   finalString[4] = (char)(((int)'0')+seconddigit);
   finalString[5] = (char)(((int)'0')+thirddigit);

   finalString[6] = '.';

   //add course to string
   unsigned int course = (decimal & 523776); //get 10 course bits
   course = course >> 9;
   thirddigit = course % 10;
   seconddigit = (course / 10) % 10;
   firstdigit = course / 100;
   finalString[7] = (char)(((int)'0')+firstdigit);
   finalString[8] = (char)(((int)'0')+seconddigit);
   finalString[9] = (char)(((int)'0')+thirddigit);

   //add grade to string
   unsigned int grade = (decimal & 448); //get 3 bits for grade
   grade = grade >> 6;
   switch (grade) {
      case 0:
         finalString[10] = 'A'; break;
      case 1:
         finalString[10] = 'B'; break;
      case 2:
         finalString[10] = 'C'; break;
      case 3:
         finalString[10] = 'D'; break;
      case 4:
         finalString[10] = 'F'; break;
      case 5:
         finalString[10] = 'I'; break;
      case 6:
         finalString[10] = 'S'; break;
      default:
         finalString[10] = 'U';
   }

   //add grade sign to string
   unsigned int sign = (decimal & 48); //get 2 bits for symbol
   sign = sign >> 4;
   if (sign == 0)
      finalString[11] = '+';
   else if (sign == 1)
      finalString[11] = '-';
   else
      finalString[11] = '/';

   //add credits to string
   unsigned int credits1 = (decimal & 14); //get 3 bits for credits1
   credits1 = credits1 >> 1;
   finalString[12] = (char)(((int)'0')+credits1);
   finalString[13] = '.';
   unsigned int credits2 = (decimal & 1); //get 1 bit for credits2
   if (credits2 == 0)
      finalString[14] = '0';
   else
      finalString[14] = '5';

   finalString[15] = '\0';

   printf("%s\n", finalString);
}
